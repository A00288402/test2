package com.micro.assessment.car.dto;

public enum Condition {
    NEW, USED;
}
