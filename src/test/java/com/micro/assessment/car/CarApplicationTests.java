package com.micro.assessment.car;

import com.micro.assessment.car.dto.Car;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CarApplicationTests {
    @LocalServerPort
    private Integer port;

    private String baseURL = "http://localhost";

    private static RestTemplate testRestTemplate = null;

    @BeforeAll
    static void init() {
        testRestTemplate = new RestTemplate();
        testRestTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });
    }

    @BeforeEach
    void setup(){
        baseURL = baseURL.concat(":").concat(port.toString()).concat("/clients");
    }

    @Test
    void shouldReturnACar(){
        String url = baseURL.concat("/1");
        Car car = testRestTemplate.getForObject(baseURL, Car.class);
        assertAll(
                () -> assertNotNull(car)
        );
    }
}
